#ifndef _COMMONS_HPP
#define _COMMONS_HPP

#include <fstream>
#include <vector>
#include <string>

namespace aoc {

class commons {
public:
    static std::vector<std::string> get_input_lines(std::string path) {
        std::vector<std::string> result;
        std::ifstream infile(path);
        std::string line;
        while(std::getline(infile, line)) {
            result.push_back(line);
        }
        return result;
    }

};

}
#endif // !_COMMONS_HPP
