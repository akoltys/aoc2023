#include <iostream>
#include <map>
#include <vector>
#include "../commons.hpp"

void part_one () {
    auto inputs = aoc::commons::get_input_lines("input.txt");
    long long int result = 0;
    for (auto line: inputs) {
        std::cout << line << '\n';
        int first = -1;
        int last  = -1;

        for (auto c: line) {
            if (c >= '0' && c <= '9') {
                c = c - '0';
                first = first == -1 ? c : first;
                last = c;
            }
        }
        std::cout << "\tfirst:" << first << '\n';
        std::cout << "\tlast:" << last << '\n';
        result += first*10 + last;

    }
    std::cout << "Result: " << result << '\n';
}

const std::map<std::string, int> number_strings = {
    { "one", 1},
    { "two", 2},
    { "three", 3},
    { "four", 4},
    { "five", 5},
    { "six", 6},
    { "seven", 7},
    { "eight", 8},
    { "nine", 9},
};

void part_two() {
    auto inputs = aoc::commons::get_input_lines("input.txt");
    //auto inputs = aoc::commons::get_input_lines("test2.txt");
    long long int result = 0;
    for (auto line: inputs) {
        if (line.at(0) == '#') {
            continue;
        }
        std::cout << line << '\n';
        int first_number_idx = -1;
        int first_number_val = -1;
        int last_number_idx = -1;
        int last_number_val = -1;
        int first_string_idx = -1;
        int first_string_val = -1;
        int last_string_idx = -1;
        int last_string_val = -1;
        // get first and last index of asci numbers in current line
        for (auto idx = 0; idx < line.length(); idx++) {
            if (line.at(idx) >= '0' && line.at(idx) <= '9') {
                if (first_number_idx == -1) {
                    first_number_idx = idx;
                    first_number_val = line.at(idx)-'0';
                }
                last_number_idx = idx;
                last_number_val = line.at(idx)-'0';
            }
        }
        //std::cout << "\tFirst number: " << first_number_idx  << " val: " << first_number_val << '\n';
        //std::cout << "\tLast number: " << last_number_idx << " val: " << last_number_val << '\n';
        first_string_idx = first_number_idx;
        last_string_idx = last_number_idx;
        // get first and last index of string numbers in current line;
        for (auto number: number_strings) {
            //std::cout << number.first << '\n';
            auto idx_first = line.find(number.first);
            auto idx_last = line.rfind(number.first);
            std::cout << number.first << " last_idx: " << idx_last << '\n';
            if (idx_first != std::string::npos && (first_string_idx == -1 || first_string_idx > idx_first)) {
                first_string_idx = idx_first;
                first_string_val = number.second;
            }
            if (idx_last != std::string::npos && (last_string_idx == -1 || last_string_idx < idx_last)) {
                last_string_idx = idx_last;
                last_string_val = number.second;
            }
        }
        //std::cout << "\tFirst string: " << first_string_idx  << " val: " << first_string_val << '\n';
        //std::cout << "\tLast string: " << last_string_idx << " val: " << last_string_val << '\n';

        auto first = 0;
        if ( first_number_val == -1) {
            first = first_string_val;
        } else if (first_string_val == -1) {
            first = first_number_val;
        } else {
            first = first_number_idx < first_string_idx ? first_number_val : first_string_val;
        }
        
        auto last = 0;
        if (last_number_val == -1) {
            last = last_string_val;
        } else if (last_string_val == -1) {
            last = last_number_val;
        } else {
            last = last_number_idx > last_string_idx ? last_number_val : last_string_val;
        }
        std::cout << "First: " << first << " Last: " << last << '\n';
        result += first*10 + last;
        std::cout << "Interresult: " << result << '\n';
    }
    std::cout << "Result: " << result << '\n';
}

int main() {
    std::cout << "AoC 2023 Day 1\n";
    //part_one();
    part_two();
    return 0;
}
